/* eslint dot-notation: 0, no-unused-vars: 0 */
/* eslint prefer-arrow-callback: "off" */

describe('REST API', function () {

	let server: any;

	beforeEach(function (done) {
		Object.keys(require.cache).forEach((key) => delete require.cache[key]);
		require('chai').should();
		const hapi = require('hapi');
		const server = hapi.Server({
			host: 'localhost',
			port: 3000
		});
		require('../routes.js')(server);
		done();
	});

	const options = {
		method: 'GET',
		url: '/item/1',
		headers: {
			'Content-Type': 'application/json'
		}
	};

	// context('when creating an item it', function () {
	// 	it('should reply it', async function () {
	// 		const response = await server.inject(options);
	// 		response.should.be.an('object').and.contain.keys('statusCode','payload');
	// 		response.statusCode.should.equal(200);
	// 		response.payload.should.be.a('string');
	// 		const payload = JSON.parse(response.payload);
	// 		payload.should.be.an('object').and.contain.keys('title', 'language');
	// 		payload.title.should.equal('Dummy');
	// 		payload.language.should.equal('en');
	// 	});
	// });
});
