/* eslint prefer-arrow-callback: "off" */
import { checkExam, convertItemsToAmountMap } from '../controllers/commonGeneratorFunctions';
import { customVector } from '../controllers/customVector';
const items = require('./testData/items.json');
import chai = require('chai');
const should = chai.should();

describe('commonGeneratorFunctions', function () {

	beforeEach(function (done) {
		Object.keys(require.cache).forEach((key) => delete require.cache[key]);
		done();
	});

	const v1 = new customVector([1,1,1]);
	const min = new customVector([1,2,3]);
	const max = new customVector([3,4,5]);
	const exam = new customVector([2,3,4]);
	const throwExam = new customVector([2,3,4,5]);
	const wrongExam = new customVector([-2,3,4]);

	context('when checking an exam', function () {
		it('should be between the min and max vectors', function () {

			checkExam(exam, min, max).should.be.true;
			checkExam(exam.add(v1), min, max).should.be.true;
			checkExam(exam.subtract(v1), min, max).should.be.true;
			checkExam(min, min, max).should.be.true;
			checkExam(max, min, max).should.be.true;
			checkExam(max.add(v1), min, max).should.be.false;
			checkExam(min.subtract(v1), min, max).should.be.false;
			should.throw(() => checkExam(throwExam, min, max));
			checkExam(wrongExam, min, max).should.be.false;
		});

		it('should throw if exam and min/max vectors got a different size', function () {

			should.throw(() => checkExam(throwExam, min, max));
			should.throw(() => checkExam(exam, min, throwExam));
			should.throw(() => checkExam(exam, throwExam, max));
			should.throw(() => checkExam(exam, throwExam, throwExam));
		});
	});

	context('when parsing items', function () {
		it('should create a correct amountMap', function () {
			const amountMap = convertItemsToAmountMap(items['@graph']);

			amountMap.get('http://tech4comp/eal/SingleChoiceItem')!.should.equal(2);
			amountMap.get('http://tech4comp/eal/remember')!.should.equal(3);
			amountMap.get('http://tech4comp/eal/factual')!.should.equal(3);
			amountMap.get('http://tech4comp/eal/MultipleChoiceItem')!.should.equal(2);
			amountMap.get('http://tech4comp/eal/FreeTextItem')!.should.equal(1);
			amountMap.get('http://tech4comp/eal/analyze')!.should.equal(1);
			amountMap.get('http://tech4comp/eal/procedural')!.should.equal(1);
			amountMap.get('http://tech4comp/eal/ArrangementItem')!.should.equal(1);
			should.equal(amountMap.get('http://tech4comp/eal/RemoteItem')!, undefined);
		});

		it('should yield an empty map if no items are passed', function () {
			const amountMap = convertItemsToAmountMap([]);

			amountMap.size.should.equal(0);
		});

		it('should respect unknown types', function () {
			const amountMap = convertItemsToAmountMap([{'@id': '', '@type': 'test.com/test', 'learningOutcome': '', 'annotations': { topics: [], bloom: [] } }]);

			amountMap.size.should.equal(1);
			amountMap.get('test.com/test')!.should.equal(1);
		});
	});

});
