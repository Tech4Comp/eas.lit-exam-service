/* eslint prefer-arrow-callback: "off" */
import { customVector } from '../controllers/customVector';
import chai = require('chai');
const should = chai.should();

describe('Vector', function() {

	beforeEach(function(done) {
		Object.keys(require.cache).forEach((key) => delete require.cache[key]);
		done();
	});

	const vzero = customVector.zeroVector(3); // eslint-disable-line
	const v0 = new customVector(null);
	const v1 = new customVector([1, 2, -2]);
	const v2 = new customVector([2, 3, -2]);
	const v3 = new customVector([2, 3, -2, -5]);
	const v4 = new customVector([-2, -3, -2, -5]);

	context('when creating a new vector', function() {
		it('should have the correct trivial values', function() {

			vzero.size.should.equal(3);
			v0.size.should.equal(0);
			v1.size.should.equal(3);
			v3.size.should.equal(4);
			v4.size.should.equal(4);

			vzero.length.should.equal(0);
			v0.length.should.equal(0);
			v1.length.should.equal(3);
			v3.length.should.equal(Math.sqrt(42));

			vzero.largestElement.should.equal(0);
			should.equal(v0.largestElement, undefined);
			v1.largestElement.should.equal(2);
			v3.largestElement.should.equal(3);
			v4.largestElement.should.equal(-2);
		});
	});

	context('when adding two vectores', function() {
		it('should yield the added vector', function() {

			vzero.add(vzero).elements.toJS().should.eql([0, 0, 0]);
			v1.add(v2).elements.toJS().should.eql([3, 5, -4]);
			v2.add(v1).elements.toJS().should.eql([3, 5, -4]);
			v1.add(v1).elements.toJS().should.eql([2, 4, -4]);
		});

		it('should throw if vectors got different size', function() {

			should.throw(() => v1.add(v3), Error, 'vectors got different size');
			should.throw(() => v3.add(v1), Error, 'vectors got different size');
			should.throw(() => v0.add(vzero), Error, 'vectors got different size');
		});
	});

	context('when subtracting two vectores', function() {
		it('should yield the subtracted vector', function() {

			vzero.subtract(vzero).elements.toJS().should.eql([0, 0, 0]);
			v1.subtract(v2).elements.toJS().should.eql([-1, -1, 0]);
			v2.subtract(v1).elements.toJS().should.eql([1, 1, 0]);
			v1.subtract(v1).elements.toJS().should.eql([0, 0, 0]);
		});

		it('should throw if vectors got different size', function() {

			should.throw(() => v1.subtract(v3), Error, 'vectors got different size');
			should.throw(() => v3.subtract(v1), Error, 'vectors got different size');
			should.throw(() => v0.subtract(vzero), Error, 'vectors got different size');
		});
	});

	context('when calculating the dotProduct of two vectores', function() {
		it('should yield the correct result', function() {

			vzero.dotProduct(vzero).should.equal(0);
			v1.dotProduct(v2).should.equal(12);
			v2.dotProduct(v1).should.equal(12);
			v1.dotProduct(v1).should.equal(9);
		});

		it('should throw if vectors got different size', function() {

			should.throw(() => v1.dotProduct(v3), Error, 'vectors got different size');
			should.throw(() => v3.dotProduct(v1), Error, 'vectors got different size');
			should.throw(() => v0.dotProduct(vzero), Error, 'vectors got different size');
		});
	});

	context('when multiplying a vector with a scalar', function() {
		it('should yield the correct result', function() {

			vzero.multiplyScalar(2).elements.toJS().should.eql([0, 0, 0]);
			v1.multiplyScalar(2).elements.toJS().should.eql([2, 4, -4]);
			v2.multiplyScalar(2).elements.toJS().should.eql([4, 6, -4]);
			v1.multiplyScalar(0).elements.toJS().should.eql([0, 0, -0]);
		});
	});

	context('when calculating the distance between two vectores', function() {
		it('should yield the correct result', function() {

			vzero.distance(vzero).should.equal(0);
			v1.distance(v2).should.equal(v1.subtract(v2).length);
			v2.distance(v1).should.equal(v2.subtract(v1).length);
			v1.distance(v1).should.equal(0);
		});

		it('should throw if vectors got different size', function() {

			should.throw(() => v1.distance(v3), Error, 'vectors got different size');
			should.throw(() => v3.distance(v1), Error, 'vectors got different size');
			should.throw(() => v0.distance(vzero), Error, 'vectors got different size');
		});
	});

	context('when comparing two vectors', function() {
		it('should recognize the larger one correctly', function() {

			vzero.largerEqualThan(vzero).should.be.true;
			v1.largerEqualThan(v2).should.be.false;
			v2.largerEqualThan(v1).should.be.true;
			v1.largerEqualThan(v1).should.be.true;
		});

		it('should recognize the smaller one correctly', function() {

			vzero.smallerEqualThan(vzero).should.be.true;
			v1.smallerEqualThan(v2).should.be.true;
			v2.smallerEqualThan(v1).should.be.false;
			v1.smallerEqualThan(v1).should.be.true;
		});

		it('should throw if vectors got different size', function() {

			should.throw(() => v1.largerEqualThan(v3), Error, 'vectors got different size');
			should.throw(() => v3.largerEqualThan(v1), Error, 'vectors got different size');
			should.throw(() => v0.largerEqualThan(vzero), Error, 'vectors got different size');
			should.throw(() => v1.smallerEqualThan(v3), Error, 'vectors got different size');
			should.throw(() => v3.smallerEqualThan(v1), Error, 'vectors got different size');
			should.throw(() => v0.smallerEqualThan(vzero), Error, 'vectors got different size');
		});
	});

});
