import hapi from '@hapi/hapi';
import { resolve } from 'path';
import config from './config';
import routes from './routes';

// @ts-ignore
global.appRoot = resolve(__dirname);

const server = new hapi.Server({
	port: config.port,
	routes: {cors: true}
});

module.exports = server;
// @ts-ignore
global.server = server;

server.ext('onPreResponse', (request, h) => {
	const response = request.response;
	if ('isBoom' in response && response.isBoom)  {
		const is4xx = response.output.statusCode >= 400 && response.output.statusCode < 500;
		if (is4xx && response.data)
			response.output.payload.data = response.data;
	}
	return h.continue;
});

const plugins = [
	require('@hapi/inert'),
	require('@hapi/vision'), {
		plugin: require('@hapi/good'),
		options: {
			ops: {
				interval: 1000
			},
			reporters: {
				console: [{
					module: '@hapi/good-squeeze',
					name: 'Squeeze',
					args: [{
						log: '*',
						response: '*',
						request: '*'
					}]
				}, {
					module: '@hapi/good-console'
				}, 'stdout']
			}
		}
	}, {
		plugin: require('hapi-swagger'),
		options: {
			host: config.domain + ((config.port !== '80') ? ':' + config.port : ''),
			info: {
				title: 'Exam API',
				description: 'Powered by node, hapi, joi, hapi-swaggered, hapi-swaggered-ui and swagger-ui',
				version: '0.1.0'
			}
		}
	}
];

async function init() {
	await server.register(plugins);
	server.route(routes);

	await server.start();
	server.log('info', 'Server started at ' + server.info.uri);

}

process.on('unhandledRejection', (err) => {
	console.error(err);
	process.exit(1);
});

init();
