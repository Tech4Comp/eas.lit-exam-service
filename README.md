# EAs.LiT Exam-Service #

This service provides a REST-API to upload item sets and generate exams from these sets. It allows to choose different algorithms to generate exams and results are always lists of item IDs.

This service was built with Typescript (in contrast to other EAs.LiT services) and makes use of type checks and type inferences.

## How to get started
1. Clone the repository via `git clone ...`.
2. Change into the directory and install all dependencies via `npm install`
3. Transpile and run the project via `npm start`. The started service is accessible via [http://localhost:3000](http://localhost:3000)

The service is now started in development mode. You may run `npm run lint` to lint all code via ESLint. Have a look at the package.json file for other available commands.

The service won't use any third party services. See see the file [docker-compose.yml](./docker-compose.yml) for available environment variables.

As soon as the service is started, an OpenAPI documentation page (swagger) is created at `/documentation` that may be used to test and experiment with this service. Test data can be found in [tests/data/](./tests/data/).

## Production Environment
Use the included dockerfile to build a docker image and the included docker-compose.yml files to start it up.

Look at the dockerfile and docker-compose.yml files to gain insights into manual deployment.

## Automatic Builds and Deployments
By including `[build]` to a commit message on the main branch, a GitLab CI pipeline is triggered and builds a docker image, which is published on GitLab. Afterwards, a deployment stage is triggered, which deploys this newly built image to the staging environment. Deployment may also be triggered on its own, by including `[deploy]` to a commit message.

## Development
The root directory contains all source code, as well as all configuration files. The easiest way to get around is to look at the routes.ts file.

All exam generation algorithms are located inside `/controllers/` and were split into different modules, implementing a common interface.

See the file `/controllers/ideas.txt` for topics on how to improve these algorithms.

## Notes

To properly install all the dependencies this service needs a working version of curl.
