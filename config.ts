import { isEmpty } from 'lodash';
import * as nodemailer from 'nodemailer';

const domain = (!isEmpty(process.env.VIRTUAL_HOST)) ? process.env.VIRTUAL_HOST : 'localhost';
const port = (!isEmpty(process.env.VIRTUAL_PORT)) ? process.env.VIRTUAL_PORT : '3000';
const protocol = 'http';
const hostname = protocol + '://' + domain + ((port === '80') ? '' : ':' + port);
console.info('Using ' + hostname + ' as hostname');

const frontend = (!isEmpty(process.env.SERVICE_URL_FRONTEND)) ? process.env.SERVICE_URL_FRONTEND : '';
const smtp = {
	host: (!isEmpty(process.env.SMTP_HOST)) ? process.env.SMTP_HOST : '',
	port: (!isEmpty(process.env.SMTP_PORT)) ? process.env.SMTP_PORT : '25',
	security: process.env.SMTP_SECURITY === 'true',
	username: (!isEmpty(process.env.SMTP_USERNAME)) ? process.env.SMTP_USERNAME : '',
	password: (!isEmpty(process.env.SMTP_PASSWORD)) ? process.env.SMTP_PASSWORD : '',
};
const fromAddress = (!isEmpty(process.env.FROM_ADDRESS)) ? process.env.FROM_ADDRESS : '';

let transporter = {};
//@ts-ignore
if(!isEmpty(smtp.host) && Number.isInteger(Number.parseInt(smtp.port)) && Number.parseInt(smtp.port) > 0 && !isEmpty(smtp.username) && !isEmpty(smtp.password) && !isEmpty(fromAddress)) {
	console.info('Using ' + smtp.host + ' as SMTP server');
	transporter = nodemailer.createTransport({
		host: smtp.host,
		//@ts-ignore
		port: Number.parseInt(smtp.port),
		secure: false,
		auth: {
			user: smtp.username,
			pass: smtp.password
		}
	});

	//@ts-ignore
	transporter.verify((error) => {
		if (error) {
			console.log('Testing the SMTP server failed, deactivating it...', error);
			transporter = {};
			module.exports.transporter = {};
		}
	});
} else
	console.info('Deactivating email capabilities as not all smtp and email data was provided');

export default {
	domain: domain,
	port: port,
	protocol: protocol,
	hostname: hostname,
	serviceURLs: {
		frontend: frontend
	},
	emailTransporter: transporter,
	fromAddress: fromAddress
};
