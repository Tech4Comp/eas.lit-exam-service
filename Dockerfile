# See https://github.com/rmeissn/tech4comp-dockerfiles for the base image description
FROM node:lts-alpine
LABEL maintainer="Roy Meissner <meissner@informatik.uni-leipzig.de>"

RUN apk add --no-cache supervisor curl && rm -rf /var/cache/apk/*

ARG BUILD_ENV=LOCAL
ENV APPLICATION_PORT=${APPLICATION_PORT:-80}

RUN mkdir /nodeApp
WORKDIR /nodeApp

COPY ./ ./
RUN if [ "$BUILD_ENV" != "CI" ] ; then rm -R node_modules ; npm install ; fi

RUN npm run build

RUN npm prune --production

RUN chmod +x ./cleanup.sh && cp ./cleanup.sh /etc/periodic/daily/cleanup

CMD supervisord -c /nodeApp/supervisord.conf
