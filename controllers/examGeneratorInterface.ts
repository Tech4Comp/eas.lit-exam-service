import { Set, List } from 'immutable';

export interface examGenerator {
	generateExams(numberOfExams: number): Promise<Set<Exam>>
}
export type Restrictions = {
	amountOfItems: number,
	[key: string]: {
		[key: string]: {min: number, max: number}
	} | number
 }

export type Exam = List<URL>

export type URL = string
