// This class uses a single-core process

import { customVector } from './customVector';
import { examGenerator as examGeneratorInterface, Restrictions, Exam, URL } from './examGeneratorInterface';
import { Set, List, Map } from 'immutable';
import { orderBy } from 'lodash';
import { convertItemToVector, convertItemsToAmountMap, createRestrictionVectors, checkExam, ItemsGraph } from './commonGeneratorFunctions';

export default class examGenerator implements examGeneratorInterface {

	private itemIDs: List<URL>
	private itemsAsVectors: Map<URL, customVector>
	private restrictions: Restrictions
	private minRestrictionsVector: customVector
	private maxRestrictionsVector: customVector
	private abortTime: number
	private validExams: Set<Exam> = Set<Exam>().asMutable()
	private startTime = 0
	private sizeOfExamBeforeChecking = 0

	constructor(items: ItemsGraph = {'@graph': []}, restrictions: Restrictions = {amountOfItems: 0}, maxTime = 60 * 1000, numberOfExams = 1) {
		this.itemIDs = List(Set(items['@graph'].map((item) => item['@id'])));
		const amountMap = convertItemsToAmountMap(items['@graph']);
		this.itemsAsVectors = Map( items['@graph'].map((item) => [ item['@id'], convertItemToVector(item, restrictions, amountMap)]) );
		this.restrictions = restrictions;
		[this.minRestrictionsVector, this.maxRestrictionsVector] = createRestrictionVectors(restrictions, items['@graph']);
		this.abortTime = maxTime;
		console.log(`Using ${this.itemIDs.size} Items to search for ${numberOfExams} exams of length ${restrictions.amountOfItems} in ${(maxTime / 60) / 1000} minutes via a greedy and early cut branches backtracking algorithm`);
	}

	async generateExams (numberOfExams = 1): Promise<Set<Exam>> {
		this.startTime = new Date().getTime();
		const zeroExamVector = this.minRestrictionsVector.multiplyScalar(0);
		this.validExams = Set<Exam>().asMutable();

		const start = new Date().getTime();
		this.backtrack(this.itemIDs, List(), zeroExamVector);// TODO any performance gains by List().asMutable? Is this possible?
		const stop = new Date().getTime();
		console.log('duration of calculation: ' + (stop-start) + 'ms');
		console.log('found ' + this.validExams.size + ' destinct exams');

		if(this.validExams.size < numberOfExams) throw new Error('toFewExamsGenerated');

		return this.validExams;
	}

	backtrack(items: List<URL>, validExam: Exam, examVector: customVector) {
		this.sizeOfExamBeforeChecking = this.minRestrictionsVector.largestElement;
		// simple greedy approach
		// this.backtrackGreedy(this.itemsAsVectors, validExam, examVector);
		console.log('Using progression');
		this.backtrackGreedy(this.itemsAsVectors, validExam, examVector, true);
	}

	/*Backtracking, that cuts branches faster by testing for restrictions early. This is useful for a lot of dense restrictions, as many produced branches will not fulfil the restrictions. Contrary this appraoch extends the runtime for few and sparse restrictions, as branches need to be tested near to the maximum depth and thus this appraoch adds a lot of operations.*/
	/*Backtracking, that chooses the next element greedy. Thus created branches will lead much faster to fulfilled restrictions and cutting branches is more effective. In contrast, this also means that the first created exams will be similar up to some extent, due to the greedy choice.*/
	//Optional progression parameter, which tells the algorithm to reach minRestrictions fast and maxRestrictions slow
	// NOTE tree width is restricted by half of items.size as last half will probably not lead to valid exams.
	backtrackGreedy(remainingItems: Map<URL, customVector>, validExam: Exam, examVector: customVector, minReached = false, useProgression = false): void {
		const vectorToUse = (!minReached) ? this.minRestrictionsVector : this.maxRestrictionsVector;
		const progressionRate = (useProgression) ? ((!minReached) ? 1 : 0) :  1;
		let remainingItemsFiltered = remainingItems;
		// filter violating items
		if(minReached)
			remainingItemsFiltered = remainingItems.filter((vec) => examVector.add(vec).smallerEqualThan(this.maxRestrictionsVector));
		if(remainingItemsFiltered.size === 0)
			return;
		const vectorsByLargestImpact = calculateRatedVectors(remainingItemsFiltered, vectorToUse, examVector, progressionRate);
		const loopSize = (remainingItemsFiltered.size > 1) ? remainingItemsFiltered.size/2 : remainingItemsFiltered.size;
		for (let index = 0; index < loopSize; index++) {

			let newMinReached = minReached === true;
			const [itemID, itemVector] = chooseNextElementGreedy (vectorsByLargestImpact, index);
			const reducedItems = remainingItemsFiltered.remove(itemID);
			const extendedExam = validExam.push(itemID);
			const extendedExamVector = examVector.add(itemVector);
			const sizeOfnewExam = extendedExam.size;
			if (sizeOfnewExam === this.restrictions.amountOfItems) {
				if(checkExam(extendedExamVector, this.minRestrictionsVector, this.maxRestrictionsVector))
					this.validExams.add(extendedExam.sort());
			} else if( sizeOfnewExam < this.restrictions.amountOfItems ) {
				if (!newMinReached && (sizeOfnewExam >= this.sizeOfExamBeforeChecking))// NOTE skips calculating minReached until minRestrictions might be reached; omits unnecessary calculations
					newMinReached = extendedExamVector.largerEqualThan(this.minRestrictionsVector);
				if (!newMinReached || extendedExamVector.smallerEqualThan(this.maxRestrictionsVector)) {
					if(new Date().getTime() - this.startTime < this.abortTime) // stop if running too long
						this.backtrackGreedy(reducedItems, extendedExam, extendedExamVector, newMinReached);
				}
			}
		}
	}
}

/*Rates remaining item vectors for their progression rate (reach restrictionsVector fast or slow) and provides a sorted array based on chosen progression. E.g. first element progresses fast or first element progresses slow*/
function calculateRatedVectors(itemVectorMap: Map<URL, customVector>, restrictionsVector: customVector, examVector: customVector, progressionRate = 1): Array<[string, [customVector, number]]> {
	// compute remaining restrictions to fulfill
	const remainingRestrictionsToFulfill = restrictionsVector.subtract(examVector);
	// compute distance between item vectors and remeining restrictions
	const ratedVectors: Map<string, [customVector, number]> = itemVectorMap.map((vec) => [vec, remainingRestrictionsToFulfill.distance(vec)]);
	const order = (progressionRate === 1) ? 'asc' : 'desc';
	// sort by specified progression (1 fast, 0 slow)
	const vectorsByLargestImpact: Array<[string, [customVector, number]]> = orderBy(ratedVectors.toArray(), ([id, [vec, distance]]) => distance, [order]);
	return vectorsByLargestImpact;
}

/*Tries to choose the next element wisely, based on provided sortedVectors (ascending order for fast progression, descending order for slow progression. Allows to choose a different element than the best one via the deviation parameter*/
function chooseNextElementGreedy (sortedVectors: Array<[string, [customVector, number]]>, deviation: number): [string, customVector] {
	return [sortedVectors[deviation][0], sortedVectors[deviation][1][0]];
}
