import { isEmpty, get } from 'lodash'; // intersection
import { customVector } from './customVector';
import { Map, Set, List, Range } from 'immutable';
import { Restrictions, Exam, URL } from './examGeneratorInterface';

export type ItemsGraph = {
	'@context'?: Record<string, unknown>,
	'@graph': Array<ItemGraph>
}

export type ItemGraph = {
	'@id': URL,
	'@type': URL,
	learningOutcome: URL,
	annotations: {
		topics: Array<URL>,
		bloom: Array<{
			performanceLevel: URL,
			knowledgeDimension: URL
		}>
	}
}

export function convertItemToVector (item: ItemGraph, restrictions: Restrictions, amountMap: Map<string, number>): customVector { // NOTE is probably named "item embedding" (similar to "sentence embedding") and are mostly sparse vectors, if lots of restrictions are used

	let itemObjectVector: {[key: string]: number} = {};
	Object.entries(restrictions).forEach(([category, categoryRestrictions]) => {
		if(typeof(categoryRestrictions) === 'object') {
			Object.entries(categoryRestrictions).forEach(([key,value]) => {
				if(!(value.min === 0 && value.max === amountMap.get(key))) {
					let hasDimension = false;
					if(category === 'amountOfItemsByType')
						hasDimension = (item['@type'] === key);
					else if (category === 'amountOfItemsByLO')
						hasDimension = (item.learningOutcome === key);
					else if (category === 'amountOfItemsByKD' || category === 'amountOfItemsByPL') {
						const dimension = ( category === 'amountOfItemsByKD') ? 'knowledgeDimension' : 'performanceLevel';
						hasDimension = get(item, 'annotations.bloom', []).some((el: {[key: string]: string}) => el[dimension] === key);
					} else if ( category === 'amountOfItemsByTopic')
						hasDimension = get(item, 'annotations.topics', []).some((el: string) => el === key);
					itemObjectVector[key] = (hasDimension) ? 1 : 0;
				}
			});
		}
	});
	if(isEmpty(itemObjectVector))
		itemObjectVector = {tmp: 1};
	return new customVector(itemObjectVector);
}

export function createRestrictionVectors (restrictions: Restrictions, items: Array<ItemGraph>): [customVector, customVector] {
	const amountMap = convertItemsToAmountMap(items);
	let minObject: {[key: string]: number} = {}, maxObject: {[key: string]: number} = {};
	Object.entries(restrictions).forEach(([category, categoryRestrictions]) => {
		if(typeof categoryRestrictions === 'object') {
			Object.entries(categoryRestrictions).forEach(([key, value]) => {
				if(!(value.min === 0 && value.max === amountMap.get(key))) {
					if(category === 'amountOfItemsByType' || category === 'amountOfItemsByLO' || category === 'amountOfItemsByKD' || category === 'amountOfItemsByPL' || category === 'amountOfItemsByTopic') {
						minObject[key] = value.min;
						maxObject[key] = value.max;
					}
				}
			});
		}
	});
	if(isEmpty(minObject)){
		minObject = {tmp: 0};
		maxObject = {tmp: Number.MAX_SAFE_INTEGER};
	}

	const result: [customVector, customVector] = [new customVector(minObject), new customVector(maxObject)];

	analyzeSolutionSpaceSize(result[0], result[1]);

	return result;
}

/* Tries to analyse the size of the solution space (~"how many exams may be found") and logs these for the time beeing. These information might be used to automatically decide for the best fitting algorithm. Idea:
small solution space: use greedy backtracking with progression (to hit the solution space)
medium solution space: use greedy backtracking or parallel backtracking, maybe evolutionary approach
large solution space: use evolutionary approach (will result in more different exams)
NOTE see ideas.txt
*/
export function analyzeSolutionSpaceSize (minRestrictionsVector: customVector, maxRestrictionsVector: customVector): void {
	const solutionSpaceVector = maxRestrictionsVector.subtract(minRestrictionsVector);
	const filteredSolutionSpace = solutionSpaceVector.elements.filter((a) => a !== 0).sort();
	const normalizedSpaceSize = solutionSpaceVector.length / maxRestrictionsVector.length;
	const normalizedSpaceWidth = (filteredSolutionSpace.first() || 0) / (filteredSolutionSpace.last() || 1);
	// const a = minRestrictionsVector.elements.filter((a) => a !== 0);
	// const b = maxRestrictionsVector.elements.filter((a) => a !== 0);
	// console.log(a.size / b.size); // usefull?

	console.log('normalized solution space size (0 (small) - 1 (large)): ' + normalizedSpaceSize);
	console.log('normalized solution space width (0 (narrow) - 1 (wide)): ' + normalizedSpaceWidth);
}

export function createBuckets (itemVectors: Map<string, customVector>, MinRestrictionVector: customVector, MaxRestrictionVector: customVector): List<Map<URL, customVector>> {
	// let buckets: List<Array<customVector>> = List(Array(MinRestrictionVector.size));
	// MinRestrictionVector.elements.forEach((minEl, index) => {
	// 	const maxEl = MaxRestrictionVector.elements.get(index);
	// 	let bucket: customVector[]|undefined = undefined;
	// 	if (minEl !== 0 && maxEl !== 0) {
	// 		// @ts-ignore
	// 		bucket = itemVectors.filter((vector) => vector.elements.get(index)! > 0);
	// 	}
	// 	if(bucket !== undefined && bucket.length && bucket.length === 0)
	// 		bucket = undefined;
	// 	if(bucket !== undefined)
	// 		buckets.push(bucket);
	// });
	// console.log(buckets.toJS(), MinRestrictionVector.elements.toJS());
	// return buckets;
	return List<Map<URL, customVector>>();
}

export function checkExam (examVector: customVector, minVector: customVector, maxVector: customVector) {
	return examVector.largerEqualThan(minVector) && examVector.smallerEqualThan(maxVector);
}

// NOTE first idea for code, not completed, quite complex to solve in general
export function filterExamsForIntersection(exams: Set<Exam> = Set<Exam>(List()), intersectionValue = 100): void {
	// const internalExams = exams.toList().toArray();
	// console.log(internalExams);
	// const result = Set<Exam>().asMutable();
	// internalExams.forEach( (exam) => {
	// 	const remainingExams = List<Exam>().asMutable();
	// 	internalExams.forEach((exam2) => {
	// 		const ratio = intersection(exam.toArray(), exam2.toArray()).length/exam.size;
	// 		if(ratio <= intersectionValue/100)
	// 			remainingExams.push(exam2);
	// 	});
	// 	remainingExams.forEach((exam) => result.add(exam))
	// });
}

export function takeRandomExams (exams: Set<Exam>, amount: number) {
	const result = Set<Exam>().asMutable();
	const ExamList = exams.toList();
	Range(0, amount).forEach(() => {
		const index = Math.floor(Math.random() * (exams.size - 1));
		result.add(ExamList.get(index)!);
	});
	return result;
}

export function takeDifferentExams (exams: Set<Exam>, amount: number, maxIntersectionBetweenExams: number) {

	const result = Set<Exam>().asMutable();
	const limit = 300000;
	let examsAsList = exams.toList();
	if(exams.size > limit) // NOTE random limit to cut calculation time
		examsAsList = examsAsList.sortBy(Math.random).take(limit);
	const examLength = examsAsList.first()!.size;
	const maxIntersection = Math.ceil((maxIntersectionBetweenExams / 100) * examLength);
	console.log(`Searching for max Intersection of ${maxIntersectionBetweenExams} % between exams, which means ${maxIntersection} items`);

	Range(0, amount).forEach(() => {
		if(result.size === 0) { // start with random exam
			const randomOne = Math.floor(Math.random() * (examsAsList.size - 1));
			result.add(examsAsList.get(randomOne)!);
		} else {
			let unionOfAllTakenExams: List<string> = result.reduce((a: Exam, b: Exam) => {
				return a.concat(b);
			}, List());
			unionOfAllTakenExams = Set(unionOfAllTakenExams).toList(); // remove duplicate entries
			const ratedExams = rateExamsForDifference(examsAsList, unionOfAllTakenExams);
			// choose exam with fewest similarities
			if(ratedExams.first()!.intersection < maxIntersection)
				result.add(ratedExams.first()!.exam);
			else
				throw new Error('intersectionViolation');
		}
	});
	return result;
}

function rateExamsForDifference (examList: List<Exam>, examToCompare: Exam) : List<{exam: Exam, intersection: number}> {
	// lower intersection indicates more different exams
	const examsSortedByIntersection = examList.map((exam) => {
		return {exam: exam, intersection: Set(exam).intersect(Set(examToCompare)).size};
	}).sortBy((record) => record.intersection);
	const intersectionList = examsSortedByIntersection.map((element) => element.intersection);
	console.log('Smallest intersection:', intersectionList.first());
	console.log('Largest intersection', intersectionList.last());
	console.log('First few:', intersectionList.toJS());
	return examsSortedByIntersection;
}

export function convertItemsToAmountMap (items: Array<ItemGraph>): Map<URL, number> {
	const result = Map<URL, number> ().asMutable();
	items.forEach((item) => {
		let key: URL = item['@type'];
		incrementInMap(key, result);
		key = item.learningOutcome;
		incrementInMap(key, result);
		if(!isEmpty(item.annotations)) {
			(item.annotations.topics || []).forEach((topic) => incrementInMap(topic, result));
			Set((item.annotations.bloom || []).map((el) => el.performanceLevel)).forEach((el) => incrementInMap(el, result));
			Set((item.annotations.bloom || []).map((el) => el.knowledgeDimension)).forEach((el) => incrementInMap(el, result));
		}
	});
	return result.asImmutable();
}

function incrementInMap(key: URL, map: Map<URL, number>) {
	if(!isEmpty(key)) {
		if(map.has(key))
			map.update(key, (value) => value! + 1);
		else
			map.set(key, 1);
	}
}
