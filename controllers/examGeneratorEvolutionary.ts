// This Class uses a single-core process
// RAM usage stays below 500MB

import { customVector } from './customVector';
import { examGenerator as examGeneratorInterface, Restrictions, Exam, URL } from './examGeneratorInterface';
import { combinations } from 'mathjs';
import { Set, List, Map, Range } from 'immutable';
import { convertItemToVector, convertItemsToAmountMap, createRestrictionVectors, ItemsGraph } from './commonGeneratorFunctions';

export default class examGenerator implements examGeneratorInterface {
	private itemIDs: Set<URL>
	private itemsAsVectors: Map<URL, customVector>
	private restrictions: Restrictions
	private maxPossibleExams: number
	private minRestrictionsVector: customVector
	private maxRestrictionsVector: customVector
	private abortTime: number
	private validExams: Set<Exam>

	constructor(items: ItemsGraph = {'@graph': []}, restrictions: Restrictions = {amountOfItems: 0}, maxTime = 60 * 1000, numberOfExams = 1) {
		this.itemIDs = Set(items['@graph'].map((item) => item['@id']));
		const amountMap = convertItemsToAmountMap(items['@graph']);
		this.itemsAsVectors = Map( items['@graph'].map((item) => [ item['@id'], convertItemToVector(item, restrictions, amountMap)]) );
		this.restrictions = restrictions;
		this.maxPossibleExams = combinations(this.itemIDs.size, this.restrictions.amountOfItems);
		[this.minRestrictionsVector, this.maxRestrictionsVector] = createRestrictionVectors(restrictions, items['@graph']);
		this.abortTime = maxTime;
		console.log(`Using ${this.itemIDs.size} Items to search for ${numberOfExams} exams of length ${this.restrictions.amountOfItems} in ${(maxTime / 60) / 1000} minutes via a evolutionary algorithm`);
		this.validExams = Set<Exam>().asMutable();
	}

	async generateExams (numberOfExams = 1): Promise<Set<Exam>> {
		this.validExams = Set<Exam>().asMutable();

		const start: number = new Date().getTime();
		// TODO hand back exams instead of side effect
		this.evolutionarySearch(this.itemIDs);
		const stop: number = new Date().getTime();
		console.log('duration of calculation: ' + (stop-start) + 'ms');
		console.log('found ' + this.validExams.size + ' destinct exams');

		if(this.validExams.size < numberOfExams) throw new Error('toFewExamsGenerated');

		return this.validExams;
	}

	/* OPTIMIZE by number of generation members and number of iterations */
	private evolutionarySearch (items: Set<URL>): void {
		const startTime = new Date().getTime();
		let currentGeneration = this.createFirstExamGeneration(items, 100);
		Range(0, 5000).forEach(() => {
			if(this.validExams.size >= this.maxPossibleExams)
				return;
			if(new Date().getTime() - startTime > this.abortTime)
				return ;
			const ratings: Set<[List<URL>, number]> = currentGeneration.map((exam) => [exam, this.rateExam(exam)]);
			const validExams = ratings.filter(([, rating]) => rating === 1).map(([exam,]) => exam.sort());
			this.validExams.union(validExams);
			const examsToMutate = this.chooseExamsToMutate(ratings);
			currentGeneration = examsToMutate.map((exam) => this.mutateExam(exam)).concat(examsToMutate.map((exam) => this.mutateExam(exam)));
		});
	}

	/* Pure random first generation OPTIMIZE with valid exams as fist generation ? */
	private createFirstExamGeneration (items: Set<URL>, amount: number): Set<Exam> {
		function fillExamRandomly(items: List<URL>, examLength: number): Exam {
			let internalItems = List(items);
			const exam = List<URL>().asMutable();
			Range(0, examLength).forEach(() => {
				const index = Math.floor(Math.random() * internalItems.size);//pick random item
				exam.push(internalItems.get(index)!);
				internalItems = internalItems.delete(index);
			});
			return exam.asImmutable();
		}
		return Range(0, amount).map(() => fillExamRandomly(List(items), this.restrictions.amountOfItems)).toSet();
	}

	/* Rates based on distance to min/max vectors. TODO is this the best rating function? TODO probably takes too much time */
	//() -> Float(0-1) - 0 is bad, 1 is valid exam
	private rateExam (exam: Exam): number {
		let rating = 0;
		const examVector = exam.map((item) => this.itemsAsVectors.get(item)!).reduce((a: customVector,b: customVector) => a.add(b));
		if(examVector.largerEqualThan(this.minRestrictionsVector)) {
			rating += 0.5;
			if(examVector.smallerEqualThan(this.maxRestrictionsVector))//=^ checkExam
				rating += 0.5;
			else {
				const distanceToMax = examVector.distance(this.maxRestrictionsVector);
				rating += (1 - distanceToMax/exam.size)/2;
			}
		} else {
			const distanceToMin = examVector.distance(this.minRestrictionsVector);
			rating = (1 - distanceToMin/exam.size)/2;
		}
		return rating;
	}

	/* Exchanges a number of random items. OPTIMIZE with smarter exchange and good enough mutation amount */
	private mutateExam (exam: Exam): Exam {
		let amountOfItemsToMutate = 1;
		if (exam.size > 5)
			amountOfItemsToMutate = Math.floor(Math.random() * (Math.floor(exam.size/5) - 1) + 1);// NOTE 1-10% of an exams size
		let remainingItems = this.itemIDs.filter((id) => !exam.includes(id)).toList();
		let newExam = exam;
		Range(0, amountOfItemsToMutate).forEach(() => {
			const indexToRemove = Math.floor(Math.random() * exam.size);
			const itemToRemove = newExam.get(indexToRemove)!;
			const indexToInsert = Math.floor(Math.random() * remainingItems.size);
			const itemToInsert = remainingItems.get(indexToInsert)!;
			newExam = newExam.delete(indexToRemove).push(itemToInsert);
			remainingItems = remainingItems.delete(indexToInsert).push(itemToRemove);
		});
		return newExam;
	}

	/* Takes the best 50% of all exams */
	private chooseExamsToMutate (ratedExams: Set<[Exam, number]>): Set<Exam> {
		return ratedExams.sortBy(([,rating]) => rating).takeLast(ratedExams.size/2).map(([exam,]) => exam);
	}

}
