import { isEmpty } from 'lodash';
import { List, Range } from 'immutable';

export interface customVector {
	elements: List<number>
  // constructor (elements: any): void
	zeroVector(size: number): customVector
	add (vector: customVector): customVector
	subtract (vector: customVector): customVector
	dotProduct(vector: customVector): number
	multiplyScalar(x: number): customVector
	distance(vector: customVector): number
	largerThan(vector: customVector): boolean
	largerEqualThan(vector: customVector): boolean
	smallerThan(vector: customVector): boolean
	smallerEqualThan(vector: customVector): boolean
}

export class customVector implements customVector {

	elements: List<number>

	constructor (elements: undefined|null|List<number>|Array<number>|customVector|unknown) {
		if(isEmpty(elements))
			this.elements = List();
		else if(elements instanceof Array )
			this.elements = List(elements);
		else if(elements instanceof customVector )
			this.elements = List(elements.elements);
		else if(elements instanceof List)
			this.elements = List(elements as List<number>);
		else if(elements instanceof Object)
			this.elements = List(Object.values(elements));
		else throw new Error('not supported');
	}

	get size (): number {
		return this.elements.size;
	}

	get length (): number {
		return Math.sqrt(this.dotProduct(this));
	}

	get largestElement (): number { // not used much, no interest in optimization
		return this.elements.sort().last();
	}

	get smallestElement (): number { // not used much, no interest in optimization
		return this.elements.sort().first();
	}

	static zeroVector(size: number): customVector  { // not used much, no interest in optimization
		return new customVector(Range(0,size).map(() => 0).toJS());
	}

	add (vector: customVector): customVector {
		checkForLength(vector, this);
		return generic(vector.elements, this.elements, (a: number,b: number) => a + b);
	}

	subtract (vector: customVector): customVector {
		checkForLength(vector, this);
		return generic(vector.elements, this.elements, (a: number,b: number) => a - b);
	}

	dotProduct(vector: customVector): number {
		checkForLength(vector, this);
		// return this.elements.zip(vector.elements).map(([a,b]) => a * b).reduce((a,b) => a + b);
		// or
		// let result = 0;
		// this.elements.forEach((el, index) => {
		// 	result += el * vector.elements.get(index);
		// });
		// return result;
		// NOTE According to jsbench.github.io, this variant is much faster than the first one (90% slower) and faster than the second one (50% slower)
		let result = 0;
		for (let i = 0; i < vector.size; i++) {
			result += vector.elements.get(i)! * this.elements.get(i)!;
		}
		return result;
	}

	multiplyScalar(x: number): customVector {
		// return new customVector(this.elements.map((a) => a * x));
		// NOTE According to jsbench.github.io, this variant is faster than the one above (55% slower)
		const result = Array(this.size);
		for (let i = 0; i < this.size; i++) {
			result[i] = this.elements.get(i)! * x;
		}
		return new customVector(result);
	}

	distance(vector: customVector): number {
		// NOTE length checked by subtract
		return this.subtract(vector).length;
	}

	largerEqualThan(vector: customVector): boolean {
		checkForLength(vector, this);
		return compare(this.elements, vector.elements, (a: number,b: number) => a >= b);
	}

	largerThan(vector: customVector): boolean {
		checkForLength(vector, this);
		return compare(this.elements, vector.elements, (a: number,b: number) => a > b);
	}

	smallerEqualThan(vector: customVector): boolean {
		checkForLength(vector, this);
		return compare( this.elements, vector.elements, (a: number,b: number) => (a <= b));
	}

	smallerThan(vector: customVector): boolean {
		checkForLength(vector, this);
		return compare( this.elements, vector.elements, (a: number,b: number) => (a < b));
	}

}

function generic (vector1: List<number>, vector2: List<number>, operation: { (a: number, b: number): void }) {
	// return new customVector(this.elements.zip(vector.elements).map(([a,b]) => operation(a,b)));
	// or
	// return new customVector(this.elements.map((a, index) => operation(a, vector.elements.get(index))));
	// NOTE According to jsbench.github.io, this variant is much faster than the first one (90% slower) and faster than the second one (50% slower)
	const result = Array(vector1.size);
	for (let i = 0; i < vector1.size; i++) {
		result[i] = operation(vector2.get(i)!, vector1.get(i)!);
	}
	return new customVector(result);
}

function checkForLength (a: customVector, b: customVector) {
	if(a.size !== b.size)
		throw new Error('vectors got different size');
}

function compare (list1: List<number>, list2: List<number>, compareFunction: { (a: number,b: number): boolean}): boolean {
	// return list1.zip(list2).map(([a,b]) => compare(a, b)).reduce((a, b) => a && b);
	// or
	// let result = list1.reduce((sum, item, i) => {
	// 	return sum && compareFunction(item, list2.get(i));
	// }, true);
	// or
	// let result = true;
	// list1.forEach((item, i) => {
	// 	result = result && compareFunction(item, list2.get(i));
	// });
	// or
	// NOTE According to jsbench.github.io, this variant is much faster than the first one (~99% slower), faster than the second one (~93% slower) and faster than the third one (90% slower)
	let result = true;
	for (let i = 0; i < list1.size; i++) {
		result = result && compareFunction(list1.get(i)!, list2.get(i)!);
	}
	return result;
}
