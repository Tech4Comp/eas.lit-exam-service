// This class uses a single-core process

import { Set, List, Map } from 'immutable';
import { customVector } from './customVector';
import { convertItemToVector, convertItemsToAmountMap, createRestrictionVectors, createBuckets, checkExam, ItemGraph, ItemsGraph } from './commonGeneratorFunctions';
import { examGenerator as examGeneratorInterface, Restrictions, Exam, URL } from './examGeneratorInterface';

export default class examGenerator implements examGeneratorInterface{

	private itemIDs: List<URL>
	private itemsAsVectors: Map<URL, customVector>
	private restrictions: Restrictions
	private minRestrictionsVector: customVector
	private maxRestrictionsVector: customVector
	private abortTime: number
	private validExams: Set<Exam> = Set<Exam>().asMutable()
	private buckets: List<Bucket>
	private startTime = 0

	constructor(items: ItemsGraph, restrictions: Restrictions = {amountOfItems: 0}, maxTime = 60 * 1000, numberOfExams = 1) {
		this.itemIDs = List(Set(items['@graph'].map((item) => item['@id'])));
		const amountMap = convertItemsToAmountMap(items['@graph']);
		this.itemsAsVectors = Map( items['@graph'].map((item) => [ item['@id'], convertItemToVector(item, restrictions, amountMap)]) );
		this.restrictions = restrictions;
		[this.minRestrictionsVector, this.maxRestrictionsVector] = createRestrictionVectors(restrictions, items['@graph']);
		this.buckets = createBuckets(this.itemsAsVectors, this.minRestrictionsVector, this.maxRestrictionsVector); // Vector(List(Vector))
		this.abortTime = maxTime;
		console.log(`Using ${this.itemIDs.size} Items to search for ${numberOfExams} exams of length ${restrictions.amountOfItems} in ${(maxTime / 60) / 1000} minutes via a bucket backtracking algorithm`);
	}

	async generateExams (numberOfExams = 1): Promise<Set<Exam>> {
		this.startTime = new Date().getTime();
		const zeroExamVector = this.minRestrictionsVector.multiplyScalar(0);
		this.validExams = Set<Exam>().asMutable();

		const start = new Date().getTime();
		await this.backtrack(List(), zeroExamVector);// TODO any performance gains by List().asMutable? Is this possible?
		const stop = new Date().getTime();
		console.log('duration of calculation: ' + (stop-start) + 'ms');
		console.log('found ' + this.validExams.size + ' destinct exams');

		if(this.validExams.size < numberOfExams) throw new Error('toFewExamsGenerated');

		return this.validExams;
	}

	async backtrack(validExam: Exam, examVector: customVector) {
		console.log('Using bucket backtracking');
		this.backtrackBuckets(validExam, examVector);
	}

	backtrackBuckets(validExam: Exam, examVector: customVector) {
		for(let i = 0; i < this.buckets.size; i++) { // instead of forEach, as for-loop is quicker to execute
			let bucketToUse = chooseMostBeneficialBucket(this.buckets, i);
			bucketToUse = bucketToUse.filter(() => true); // filter out elements violating min/max & elements in exam
			// const item = bucketToUse.get(getRandomInt(bucketToUse.size - 1));
			// const extendedExam = validExam.push(item);
			// const extendedExamVector = examVector.add(this.itemsAsVectors.get(item));
			// if (extendedExam.size === this.restrictions.amountOfItems) {
			// 	if(checkExam(extendedExamVector, this.minRestrictionsVector, this.maxRestrictionsVector))
			// 		this.validExams.add(extendedExam.sort());
			// } else if( extendedExam.size < this.restrictions.amountOfItems ) {
			// 	if(new Date().getTime() - this.startTime < this.abortTime) // stop if running too long
			// 		this.backtrackBuckets(extendedExam, extendedExamVector);
			// }
		}
	}

}

type Bucket = Map<URL,customVector>

function chooseMostBeneficialBucket (buckets: List<Bucket>, index: number): Bucket {
	return Map<URL,customVector>();
}

function getRandomInt (max: number): number {
	return Math.floor(Math.random() * max);
}
