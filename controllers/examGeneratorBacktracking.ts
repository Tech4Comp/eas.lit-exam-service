// This Class uses a single-core process

import { customVector } from './customVector';
import { examGenerator as examGeneratorInterface, Restrictions, Exam, URL } from './examGeneratorInterface';
import { Set, List, Map } from 'immutable';
import { convertItemToVector, convertItemsToAmountMap, createRestrictionVectors, checkExam, ItemsGraph } from './commonGeneratorFunctions';

export default class examGenerator implements examGeneratorInterface {

	private itemIDs: List<URL>
	private itemsAsVectors: Map<URL, customVector>
	private restrictions: Restrictions
	private minRestrictionsVector: customVector
	private maxRestrictionsVector: customVector
	private abortTime: number
	private validExams: Set<Exam> = Set<Exam>().asMutable()
	private startTime = 0

	constructor(items: ItemsGraph = {'@graph': [] }, restrictions: Restrictions = {amountOfItems: 0}, maxTime = 60 * 1000, numberOfExams = 1) {
		this.itemIDs = List(Set(items['@graph'].map((item) => item['@id'])));
		const amountMap = convertItemsToAmountMap(items['@graph']);
		this.itemsAsVectors = Map( items['@graph'].map((item) => [ item['@id'], convertItemToVector(item, restrictions, amountMap)]) );
		this.restrictions = restrictions;
		[this.minRestrictionsVector, this.maxRestrictionsVector] = createRestrictionVectors(restrictions, items['@graph']);
		this.abortTime = maxTime;
		console.log(`Using ${this.itemIDs.size} Items to search for ${numberOfExams} exams of length ${restrictions.amountOfItems} in ${(maxTime / 60) / 1000} minutes via a early cut branches backtracking algorithm`);
	}

	async generateExams (numberOfExams = 1): Promise<Set<Exam>> {
		this.startTime = new Date().getTime();
		const zeroExamVector = this.minRestrictionsVector.multiplyScalar(0);
		this.validExams = Set<Exam>().asMutable();

		const start = new Date().getTime();
		// this.backtrackSimple(this.itemIDs, List(), zeroExamVector);
		this.backtrackCutBranches(this.itemIDs, List(), zeroExamVector);
		const stop = new Date().getTime();
		console.log('duration of calculation: ' + (stop-start) + 'ms');
		console.log('found ' + this.validExams.size + ' destinct exams');

		if(this.validExams.size < numberOfExams) throw new Error('toFewExamsGenerated');

		return this.validExams;
	}

	/*Tries each and every possibility and only restricts the tree depth by the the amount of items used in exams.*/
	backtrackSimple (items: List<URL>, validExam: Exam, examVector: customVector): void {
		for(let i = 0; i < items.size; i++) { // instead of forEach, as for-loop is quicker to execute
			const item = items.get(i)!;
			const reducedItems = items.delete(i);
			const extendedExam = validExam.push(item);
			const extendedExamVector = examVector.add(this.itemsAsVectors.get(item)!);
			if (extendedExam.size === this.restrictions.amountOfItems) {
				if(checkExam(extendedExamVector, this.minRestrictionsVector, this.maxRestrictionsVector))
					this.validExams.add(extendedExam.sort());
			} else if( extendedExam.size < this.restrictions.amountOfItems ) {
				if(new Date().getTime() - this.startTime < this.abortTime) // stop if running too long
					this.backtrackSimple(reducedItems, extendedExam, extendedExamVector);
			}
		}
	}

	/*Backtracking, that cuts branches faster by testing for restrictions early. This is useful for a lot of dense restrictions, as many produced branches will not fulfil the restrictions. Contrary this appraoch extends the runtime for few and sparse restrictions, as branches need to be tested near to the maximum depth and thus this appraoch adds a lot of operations.*/
	backtrackCutBranches(items: List<URL>, validExam: Exam, examVector: customVector, minReached = false): void {
		for(let i = 0; i < items.size; i++) { // instead of forEach, as for-loop is quicker to execute
			const item = items.get(i)!;
			const reducedItems = items.delete(i);
			const extendedExam = validExam.push(item);
			const extendedExamVector = examVector.add(this.itemsAsVectors.get(item)!);
			if (extendedExam.size === this.restrictions.amountOfItems) {
				if(checkExam(extendedExamVector, this.minRestrictionsVector, this.maxRestrictionsVector))
					this.validExams.add(extendedExam.sort());
			} else if( extendedExam.size < this.restrictions.amountOfItems ) {
				let newMinReached = false;
				if (!minReached)
					newMinReached = extendedExamVector.largerEqualThan(this.minRestrictionsVector);
				if (!newMinReached || extendedExamVector.smallerEqualThan(this.maxRestrictionsVector)) {//cut branches fast
					if(new Date().getTime() - this.startTime < this.abortTime) // stop if running too long
						this.backtrackCutBranches(reducedItems, extendedExam, extendedExamVector, newMinReached);
				}
			}
		}
	}
}
