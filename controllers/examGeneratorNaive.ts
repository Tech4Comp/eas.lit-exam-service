import 'lodash.permutations';
// @ts-ignore
import { permutations } from 'lodash';
import { Set, List } from 'immutable';
import { customVector } from './customVector';
import { convertItemToVector, convertItemsToAmountMap, createRestrictionVectors, checkExam, ItemsGraph, ItemGraph } from './commonGeneratorFunctions';
import { examGenerator as examGeneratorInterface, Exam, URL } from './examGeneratorInterface';

export default class examGenerator implements examGeneratorInterface {
	private itemIDList: Array<string>
	private itemsAsVectors: Map<string|URL, customVector>
	private restrictions: { amountOfItems?: number }
	private minRestrictionsVector: customVector
	private maxRestrictionsVector: customVector

	constructor(items: ItemsGraph = {'@graph': []}, restrictions = {amountOfItems: 0}, maxTime = 60 * 1000, numberOfExams = 1) {
		this.itemIDList = items['@graph'].map((item: ItemGraph) => item['@id']);
		const amountMap = convertItemsToAmountMap(items['@graph']);
		this.itemsAsVectors = new Map(items['@graph'].map((item: ItemGraph) => [item['@id'], convertItemToVector(item, restrictions, amountMap)]));
		this.restrictions = restrictions;
		[this.minRestrictionsVector, this.maxRestrictionsVector] = createRestrictionVectors(restrictions, items['@graph']);
		console.log(`Using ${this.itemIDList.length} Items to search for ${numberOfExams} exams of length ${this.restrictions.amountOfItems} in ${(maxTime / 60) / 1000} minutes via a permutation algorithm`);
	}

	async generateExams(numberOfExams = 1): Promise<Set<Exam>> {
		// Get all permutations of allPermutationsOfExamLength with given length
		// WARNING fails fast with out of memory
		const allPermutationsOfExamLength: Array<Exam> = permutations(this.itemIDList, this.restrictions.amountOfItems);

		// Sort Permutations
		let allPermutationsOfExamLengthAsList: Array<Exam>|null = allPermutationsOfExamLength.map((list) => List(list).sort());
		console.log('found ' + allPermutationsOfExamLengthAsList.length + ' exams');

		// Eliminate duplicates
		// WARNING fails fast with call stack exceeded
		let allExams: Set<Exam>|null = Set.of(...allPermutationsOfExamLengthAsList);
		console.log('found ' + allExams.size + ' destinct exams');
		allPermutationsOfExamLengthAsList = null; //hopefully faster garbage collection

		// Eliminate entries not matching restrictions
		const matchingExams: Set<Exam> = allExams.filter((exam) => this.isExamInRestrictions(exam));
		allExams = null;//hopefully faster garbage collection

		if (matchingExams.size < numberOfExams) throw new Error('toFewExamsGenerated');

		return matchingExams;
	}

	private isExamInRestrictions(exam: Exam) {
		const examAsVectors: List<customVector> = exam.map((itemID) => this.itemsAsVectors.get(itemID)!);
		const examVector: customVector = examAsVectors.reduce((acc, curr) => acc.add(curr));
		return checkExam(examVector, this.minRestrictionsVector, this.maxRestrictionsVector);
	}

}
