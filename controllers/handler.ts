import { entityTooLarge, badRequest, badImplementation, notImplemented } from '@hapi/boom';
import fs from 'fs';
import child from 'child_process';
import { isEmpty, head } from 'lodash';
// import examGeneratorNaive from'./examGeneratorNaive';
import examGeneratorBacktracking from './examGeneratorBacktracking';
import examGeneratorGreedyBacktracking from './examGeneratorGreedyBacktracking';
// import examGeneratorBucketBacktracking from './examGeneratorBucketBacktracking';
import examGeneratorParallelBacktracking from './examGeneratorParallelBacktracking';
import examGeneratorEvolutionary from './examGeneratorEvolutionary';
import { examGenerator } from './examGeneratorInterface';
import { ItemsGraph, takeDifferentExams } from './commonGeneratorFunctions'; // takeRandomExams
import config from '../config';
import { Set, List } from 'immutable';
import { Request, ResponseToolkit } from '@hapi/hapi';
import { Restrictions } from './examGeneratorInterface';
// import { combinations } from 'mathjs'
// import { convertItemsToAmountMap, filterExamsForIntersection } from './commonGeneratorFunctions'


type md5sum = string
const items = new Map() as Map<md5sum,{created: number, items: ItemsGraph}>; // keeps all uploaded Items in Memory

export default {

	async saveItems (request: Request) {

		// @ts-ignore
		const uploadedFile = request.payload.path as fs.PathLike;
		if(isEmpty(request.payload)){
			return entityTooLarge('Seems like the payload was to large - 100MB max');
		// @ts-ignore
		} else if (request.payload.bytes <= 1) { //no payload
			fs.unlinkSync(uploadedFile);
			return badRequest('A payload is required');
		} else {
			const sessionID = head(child.execSync('md5sum ' + uploadedFile).toString().split(' ')) as md5sum;
			const path = `/tmp/${sessionID}.json` as string;
			try {
				if (items.has(sessionID)) {
					fs.unlinkSync(uploadedFile);
					return { sessionID: sessionID };
				} else if (fs.existsSync(path))
					fs.unlinkSync(path);
				fs.renameSync(uploadedFile, path);
				setItemsInMap (sessionID, path);
				return { sessionID: sessionID };
			} catch(error: any) {
				console.error(error);
				fs.unlinkSync(path);
				if(error.message === 'noItems')
					return badRequest();
				request.log('error', error);
				return badImplementation();
			}
		}
	},

	deleteItems (request: Request, h: ResponseToolkit) {
		const sessionID = request.params.sessionID as md5sum;

		if(items.has(sessionID))
			items.delete(sessionID);
		try {
			if(fs.existsSync(`/tmp/${sessionID}.json`)) {
				fs.unlinkSync(`/tmp/${sessionID}.json`);
				return h.response();
			}
		} catch(error: any) {
			console.error(error);
			request.log('error', error);
			return badImplementation();
		}
	},

	triggerAutomaticDeleteItems (request: Request) {
		try {
			return deleteItemsAll24h();
		} catch(error: any) {
			console.error(error);
			request.log('error', error);
			return badImplementation();
		}
	},

	getAmountOfPossibleExams (request: Request) {
		const sessionID = request.params.sessionID as md5sum;
		if (!correctSessionID(sessionID))
			return badRequest();

		return notImplemented();

		// const restrictions = request.payload;
		// const internalItems = items.get(sessionID).items['@graph'];
		// const amountMap = convertItemsToAmountMap(internalItems);
		// const amountWithoutRestrictions = combinations(internalItems.length, restrictions.amountOfItems);
		//
		// //TODO this currently tries to subtract from all combinations. It might be better to have X sums to calculate the amount, as shown by Katja
		// let [,...tmp] = Object.entries(restrictions);
		// tmp = tmp.flatMap(([category, categoryRestrictions]) => {
		// 	if(category === 'amountOfItemsByType') {
		// 		return Object.entries(categoryRestrictions).map(([type,{min, max}]) => {
		// 			if(min === 0 && max === 0 || (min === 0 && max === amountMap.get(type)))
		// 				return null;
		// 			else {
		// 				const synteticMax = (restrictions.amountOfItems >= max) ? max : restrictions.amountOfItems;
		// 				return range(min, synteticMax + 1).map((x) => [amountMap.get(type), x]);
		// 			}
		// 		}).filter((s) => s !== null);
		// 	} else
		// 		return null;
		// }).filter((s) => s !== null);
		//
		// tmp = [amountWithoutRestrictions,...tmp].reduce((minuend,b) => minuend - b);
		// const endResult = amountWithoutRestrictions - ((tmp === amountWithoutRestrictions) ? 0 : tmp);
		//
		// return (endResult > 0) ? endResult : 1;
	},

	async createExams (request: Request) {
		// NOTE similarity between exams/items by their magnitude and direction, use cosine similarity
		const numberOfExams = request.query.numberOfExams as number;
		const maxIntersectionBetweenExams = request.query.intersection as number;
		const restrictions = request.payload as Restrictions;
		const email = request.query.email as string;
		let maxTime= request.query.maxTime as number;
		maxTime = ((maxTime <= 60) ? maxTime : 60) * 60 * 1000; // max 60 min
		const sessionID = request.params.sessionID as md5sum;
		const itemsForGenerator = (items.get(sessionID) || {}).items;

		if (!correctSessionID(sessionID))
			return badRequest();
		else if(itemsForGenerator !== undefined) {
			try {
				const examGenerator = createExamGenerator(itemsForGenerator, restrictions, maxTime, numberOfExams, request.query.fastCalculation as boolean);
				let exams = await examGenerator.generateExams(numberOfExams);
				if(exams.size < numberOfExams) throw new Error('toFewExamsGenerated');
				exams = takeDifferentExams(exams, numberOfExams, maxIntersectionBetweenExams);
				await sendMail(email, exams);
				return exams.toJS();
			} catch (error: any) {
				if(error.message === 'toFewExamsGenerated' || error.message === 'intersectionViolation'){
					await sendMail(email, Set(), error.message);
					return badRequest(error.message);
				} else {
					console.log(error);
					return badImplementation();
				}
			}
		}
	}
};

function createExamGenerator (items: ItemsGraph, restrictions: Restrictions, maxTime: number, numberOfExams: number, fastCalculation: boolean): examGenerator {
	const amountOfItems = items['@graph'].length;
	if(fastCalculation)
		return new examGeneratorEvolutionary(items, restrictions, maxTime, numberOfExams);
	else if(restrictions.amountOfItems <= 5)
		return new examGeneratorBacktracking(items, restrictions, maxTime, numberOfExams);
	else if(amountOfItems <= 50)
		return new examGeneratorParallelBacktracking(items, restrictions, maxTime, numberOfExams);
	else
		return new examGeneratorGreedyBacktracking (items, restrictions, maxTime, numberOfExams);
}

function correctSessionID (sessionID: md5sum): boolean {
	return items.has(sessionID) && fs.existsSync(`/tmp/${sessionID}.json`);
}

function setItemsInMap (sessionID: md5sum, filePath: fs.PathLike): void {
	const itemsFromFile = JSON.parse(fs.readFileSync(filePath, 'utf8'));
	if(isEmpty(itemsFromFile['@graph'])) throw new Error('noItems');
	const currentTime = new Date().getTime();
	console.log(sessionID);
	items.set(sessionID, {created: currentTime, items: itemsFromFile});
}

async function deleteItemsAll24h () {
	const currentTime = new Date().getTime();
	items.forEach((value, key) => {
		if(currentTime - value.created >= 86400000) {
			// @ts-ignore
			global.server.inject({method: 'DELETE', url: '/items/' + key});
		}
	});
	return 'success';
}

async function sendMail (email: string, exams: Set<List<string>>, reason = '') {
	if(!isEmpty(config.emailTransporter) && !isEmpty(email)) {
		const toSend = {
			from: `"EAs.LiT" <${config.fromAddress}>`,
			to: email,
			subject: 'Prüfungen erzeugt',
			html: `<p>EAs.LiT hat Prüfungen entsprechend Ihrer Vorgaben erzeugt. Die entsprechende Datei finden sie im Anhang dieser E-Mail. Bitte laden sie diese Datei in der Prüfungserzeugungsansicht von EAs.LiT über den Knopf "Pürfungsvorlage laden" hoch, um die Prüfungen in einem von ihnen gewünschtem Format zusammenzustellen. <a href="${config.serviceURLs.frontend}">zu EAs.LiT</a></p>`,
			attachments: [{filename: 'Prüfungen ' + (new Date()).toUTCString() + '.json', content: JSON.stringify(exams.toJS()), contentType: 'application/json'}]
		};
		if(exams.size === 0) {
			toSend.attachments = [];
			toSend.subject = 'Es wurden zu wenige Prüfungen erzeugt';
			if(reason === 'intersectionViolation')
				toSend.html = `<p>Es war EAs.LiT möglich genug Prüfungen zu erzeugen, allerdings konnte die Einschränkung zur Überschneidung der Prüfungen nicht erfüllt werden. Bitte passen sie die Einschränknugen an und versuchen sie es erneut. <a href="${config.serviceURLs.frontend}">zu EAs.LiT</a></p>`;
			else
				toSend.html = `<p>Leider konnte EAs.LiT nicht genug Prüfungen erzeugen. Bitte passen sie die Einschränknugen an und versuchen sie es erneut. <a href="${config.serviceURLs.frontend}">zu EAs.LiT</a></p>`;
		}
		try {
			// @ts-ignore
			await config.emailTransporter.sendMail(toSend);
		} catch (error) {
			console.log(error);
		}
	}
}
