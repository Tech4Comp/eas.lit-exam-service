// This class uses multi-core processes

import { customVector } from './customVector';
import { examGenerator as examGeneratorInterface, Restrictions, Exam, URL } from './examGeneratorInterface';
import { Set, List, Map } from 'immutable';
import { convertItemToVector, convertItemsToAmountMap, createRestrictionVectors, ItemsGraph } from './commonGeneratorFunctions';
import Parallel from 'paralleljs';

export default class examGenerator implements examGeneratorInterface {

	private itemIDs: List<URL>
	private itemsAsVectors: Map<URL, customVector>
	private restrictions: Restrictions
	private minRestrictionsVector: customVector
	private maxRestrictionsVector: customVector
	private abortTime: number
	private validExams: Set<Exam> = Set<Exam>().asMutable()
	private startTime = 0

	constructor(items: ItemsGraph = { '@graph': [] }, restrictions: Restrictions = { amountOfItems: 0 }, maxTime = 60 * 1000, numberOfExams = 1) {
		this.itemIDs = List(Set(items['@graph'].map((item) => item['@id'])));
		const amountMap = convertItemsToAmountMap(items['@graph']);
		this.itemsAsVectors = Map(items['@graph'].map((item) => [item['@id'], convertItemToVector(item, restrictions, amountMap)]));
		this.restrictions = restrictions;
		[this.minRestrictionsVector, this.maxRestrictionsVector] = createRestrictionVectors(restrictions, items['@graph']);
		this.abortTime = maxTime;
		console.log(`Using ${this.itemIDs.size} Items to search for ${numberOfExams} exams of length ${restrictions.amountOfItems} in ${(maxTime / 60) / 1000} minutes via a parallel and early cut branches backtracking algorithm`);
	}

	async generateExams(numberOfExams = 1): Promise<Set<Exam>> {
		this.startTime = new Date().getTime();
		this.validExams = Set<Exam>().asMutable();

		const start = new Date().getTime();
		await this.backtrackParallel(this.itemIDs);
		const stop = new Date().getTime();
		console.log('duration of calculation: ' + (stop - start) + 'ms');
		console.log('found ' + this.validExams.size + ' destinct exams');

		if (this.validExams.size < numberOfExams) throw new Error('toFewExamsGenerated');

		return this.validExams;
	}

	/*Creates X tree roots (=X items) and processes all trees in parallel.*/
	//NOTE uses custom backtrackCutBranches
	async backtrackParallel(items: List<URL>) {
		function backtrackCutBranches(this: any, items: List<URL>, newExam: Exam, examVector: customVector, minReached: boolean): void {
			const myglobal = global as typeof globalThis & env;
			if (newExam.size === myglobal.env.examLength) {
				if (this.checkExam(examVector, this.minRestrictionsVector, this.maxRestrictionsVector))
					this.validExams.add(newExam.sort());
			} else if (newExam.size < myglobal.env.examLength) {
				for (let i = 0; i < items.size; i++) { // instead of forEach, as for-loop is quicker to execute
					const item = items.get(i)!;
					const reducedItems = items.delete(i);
					const extendedExam = newExam.push(item);
					const extendedExamVector = examVector.add(this.itemsAsVectors.get(item)!);
					let newMinReached = false;
					if (!minReached)
						newMinReached = extendedExamVector.largerEqualThan(this.minRestrictionsVector);
					if (!newMinReached || extendedExamVector.smallerEqualThan(this.maxRestrictionsVector)) {//cut branches fast
						if (new Date().getTime() - myglobal.env.startTime < myglobal.env.abortTime) // stop if running too long
							backtrackCutBranches(reducedItems, extendedExam, extendedExamVector, newMinReached);
					}
					// });
				}
			}
		}

		//prepareTreeAndBacktrack(String = itemID = tree root)
		function prepareTreeAndBacktrack(this: any, item: string) {
			const myglobal = global as typeof globalThis & env;
			const { Set, List, Map } = require('immutable');
			const { customVector } = require(myglobal.env.path + '/customVector');
			const { checkExam } = require(myglobal.env.path + '/commonGeneratorFunctions');
			this.checkExam = checkExam;

			//hydrate data
			this.minRestrictionsVector = new customVector(myglobal.env.minRestrictionsVector);
			this.maxRestrictionsVector = new customVector(myglobal.env.maxRestrictionsVector);
			// @ts-ignore
			this.itemsAsVectors = Map(myglobal.env.itemsAsVectors).mapEntries(([key, value]) => [key, new customVector(value.elements)]);
			const items = List(Set(myglobal.env.items).delete(item));
			const validExam = List([item]);
			const examVector = this.minRestrictionsVector.multiplyScalar(0).add(this.itemsAsVectors.get(item));
			const minReached = examVector.largerEqualThan(this.minRestrictionsVector);
			this.validExams = Set().asMutable();
			if (new Date().getTime() - myglobal.env.startTime < myglobal.env.abortTime)
				backtrackCutBranches(items, validExam, examVector, minReached);
			if (this.validExams.size > 5000) // temporary fix for https://github.com/parallel-js/parallel.js/issues/234 , 5.000 is a random value, as above 30.000 raises the error
				this.validExams = this.validExams.take(5000);
			return this.validExams.toJS();
		}

		type env = {
			env: {
				items: Array<URL>,
				examLength: number,
				minRestrictionsVector: Array<number>,
				maxRestrictionsVector: Array<number>,
				itemsAsVectors: { [key: string]: customVector },
				startTime: number,
				abortTime: number,
				path: string
			}
		}


		// @ts-ignore
		const validExams: Array<Array<URL>> = await (new Parallel(items.toList().toJS(), {
			// @ts-ignore
			env: {//dehydrate data
				items: items.toJS(),
				examLength: this.restrictions.amountOfItems,
				minRestrictionsVector: this.minRestrictionsVector.elements.toJS(),
				maxRestrictionsVector: this.maxRestrictionsVector.elements.toJS(),
				itemsAsVectors: this.itemsAsVectors.toJS(),
				startTime: new Date().getTime(),
				abortTime: this.abortTime,
				path: __dirname
			} as env
		}).require(backtrackCutBranches)) // make function available to workers
			.map(prepareTreeAndBacktrack) // run for every item as tree root
			// @ts-ignore
			.reduce(([left, right]) => left.concat(right));
		const newValidExams = Set(validExams.map((exam) => List(exam))); // rehydrate & eliminate duplicates
		this.validExams.union(newValidExams);
	}

}
