import Joi from 'joi';
import handler from './controllers/handler';

const sessionIDModel = Joi.object({
	sessionID: Joi.string().trim().token().description('sessionID from /items').required()
});

const minMaxObject = Joi.object().keys({
	min: Joi.number().integer().min(0),
	max: Joi.number().integer().min(0)
}).label('MinMaxRestriction');

const restrictionsModel = Joi.object().keys({
	amountOfItems: Joi.number().integer().positive().example(30),
	amountOfItemsByType: Joi.object().pattern(
		Joi.string().trim().uri().example('http://tech4comp/eal/SingleChoiceItem'),
		minMaxObject
	),
	amountOfItemsByKD: Joi.object().pattern(
		Joi.string().trim().uri().example('http://test.de'),
		minMaxObject
	),
	amountOfItemsByPL: Joi.object().pattern(
		Joi.string().trim().uri().example('http://test.de'),
		minMaxObject
	),
	amountOfItemsByLO: Joi.object().pattern(
		Joi.string().trim().uri().example('http://test.de'),
		minMaxObject
	),
	amountOfItemsByTopic: Joi.object().pattern(
		Joi.string().trim().uri().example('http://test.de'),
		minMaxObject
	),
}).label('Restrictions');

export default [

	{
		method: 'POST',
		path: '/items',
		config: {
			handler: handler.saveItems,
			tags: ['api'],
			description: 'Saves posted items for 24h and returns a sessionID',
			payload: {
				output: 'file',
				uploads: '/tmp/',
				maxBytes: 104857600, //100MB
				failAction: 'log'
			},
			validate: {
				payload: Joi.required(),
				headers: Joi.object({
					'content-type': Joi.string().required().valid('application/json').description('Mime-Type of the uploaded file'),
				}).unknown(),
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json'],
					produces: ['application/json'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'DELETE',
		path: '/items/{sessionID}',
		config: {
			handler: handler.deleteItems,
			tags: ['api'],
			description: 'Delete previously saved items by their sessionID from /items',
			validate: {
				params: sessionIDModel
			},
			plugins: {
				'hapi-swagger': {
					responses: {
						204: {}
					}
				}
			}
		}
	},

	{
		method: 'DELETE',
		path: '/items',
		config: {
			handler: handler.triggerAutomaticDeleteItems,
			tags: ['api'],
			description: 'Delete all saved items if they are older than 24h',
			plugins: {
				'hapi-swagger': {
					responses: {
						204: {},
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/exams/{sessionID}/amountOfPossibleExams',
		config: {
			handler: handler.getAmountOfPossibleExams,
			tags: ['api'],
			description: 'Get the amount of possible exams for a sessionID and restrictions document',
			validate: {
				params: sessionIDModel,
				payload: restrictionsModel
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json'],
					produces: ['application/json'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	},

	{
		method: 'POST',
		path: '/exams/{sessionID}',
		config: {
			handler: handler.createExams,
			tags: ['api'],
			description: 'Create exams for a sessionID and restrictions document',
			validate: {
				params: sessionIDModel,
				payload: restrictionsModel,
				query: Joi.object({
					numberOfExams: Joi.number().positive().max(50).description('The number of exams to create').required(),
					fastCalculation: Joi.boolean().default(false).description('Use faster calculation method, not finding all exams'),
					email: Joi.string().allow('').trim().email().description('E-Mail, at which to send the results'),
					intersection: Joi.number().integer().min(0).max(100).default(100).description('Item intersection between two exams in %'),
					maxTime: Joi.number().integer().min(1).max(60).default(30).description('Maximum calculation time in min')
				})
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['application/json'],
					responses: {
						200: {},
						400: {
							description: 'Probably a parameter is missing or not allowed'
						},
						415: {
							description: 'The accept header is not supported'
						},
						422: {
							description: 'One of the data type checks failed, see message'
						}
					}
				}
			}
		}
	}

];
